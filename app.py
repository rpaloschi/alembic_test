from flask import Flask
from core import db


def create_app():
    new_app = Flask(__name__)
    # https://github.com/mitsuhiko/flask-sqlalchemy/issues/82
    db.app = new_app
    db.init_app(new_app)
    return new_app


if __name__ == "__main__":
    app = create_app()
    db.create_all()

    @app.route("/")
    def hello():
        return "Hello World!"

    app.run()
